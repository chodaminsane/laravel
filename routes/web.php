<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $nama = "INDEX CHODAM ALIEF";
//     return view('index', ['nama' => $nama]);
// });

// Route::get('/about', function () {
//     $nama = "HALAMAN ABOUT";
//     return view('about', ['nama' => $nama]);
// });

// Route::get('/mahasiswa', function () {
//     $nama = "HALAMAN MAHASISWA";
//     return view('mahasiswa', ['nama' => $nama]);
// });
Route::get('/', 'App\Http\Controllers\PagesController@home');
Route::get('/about', 'App\Http\Controllers\PagesController@about');

Route::get('/mahasiswa', 'App\Http\Controllers\MahasiswaController@index');


//Students (posisi route atas bawah berpengaruh terhadap proses)
Route::get('/students', 'App\Http\Controllers\StudentsController@index');
Route::get('/students/create', 'App\Http\Controllers\StudentsController@create');
Route::get('/students/{student}', 'App\Http\Controllers\StudentsController@show');
Route::post('/students', 'App\Http\Controllers\StudentsController@store');





