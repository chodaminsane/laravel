@extends('layout/main')

@section('title', 'Daftar Student')

@section('container')
  <div class="container">
    <div class="row">
      <div class="col-6">
        <h1 class="mt-4">Daftar Student</h1>
        
        <a href="/students/create" class="btn btn-primary my-3">Tambah </a>

        @foreach( $students as $student )
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-items-center">
            {{ $student->nama }}
            <a href="/students/{{ $student->id }}" class="badge badge-primary">Detail</a>
          </li>
        </ul>
        @endforeach
      </div>
    </div>
  </div>
@endsection