@extends('layout/main')

@section('title', 'Daftar Student')

@section('container')
  <div class="container">
    <div class="row">
      <div class="col-6">
        <h1 class="mt-4">Detail Student</h1>

        <div class="card">
          <div class="card-body">
            <h5 class="card-title">{{$student->nama}}</h5>
            <h6 class="card-subtitle mb-2 text-muted">{{ $student->email }}</h6>
            <p class="card-text">{{ $student->jurusan }}</p>

            <button type="submit" class="btn btn-primary">Edit</button>
            <button type="submit" class="btn btn-danger">Delete</button>
            <br>
            <a href="/students" class="card-link">back</a>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection