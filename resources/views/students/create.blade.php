@extends('layout/main')

@section('title', 'Daftar Student')

@section('container')
  <div class="container">
    <div class="row">
      <div class="col-8">
        <h1 class="mt-4">Form Tambah Student</h1>
        
        <!-- pakai /students akan diarahkan oleh routes dengan method post. mengakses method store di controller -->
        <form method="post" action="/students">
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" placeholder="Masukkan nama mahasiswa" name="nama">
          </div>
          <div class="form-group">
            <label for="npm">npm</label>
            <input type="text" class="form-control" id="npm" placeholder="Masukkan npm mahasiswa" name="npm">
          </div>
          <div class="form-group">
            <label for="email">email</label>
            <input type="text" class="form-control" id="email" placeholder="Masukkan email mahasiswa" name="email">
          </div>
          <div class="form-group">
            <label for="jurusan">jurusan</label>
            <input type="text" class="form-control" id="jurusan" placeholder="Masukkan jurusan mahasiswa" name="jurusan">
          </div>

          <button type="submit" class="btn btn-primary">Submit!</button>
         
        </form>

      </div>
    </div>
  </div>
@endsection